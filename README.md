# Learning Python ![Python](tabler-icon-brand-python.svg)

![](https://img.shields.io/badge/Python-Beginner-blue?style=flat-square&logo=python&logoColor=white)

[![](https://img.shields.io/badge/Ko--fi-Support-blue?style=flat-square&logo=ko-fi)](https://ko-fi.com/bbbhltz)

*A Collection of Resources for Learning Python*

## Introduction

This is my attempt at learning Python.

As a teacher, I am used to operating with a lesson plan, or syllabus.

I plan on working on this between 7 and 10 hours per week. As I have no prior programming experience, I will need to take my time.

Please note: It is with great difficulty that I write this using American English. There is nothing wrong with American English, but I will likely make mistakes as I am used to using British spellings for words. Any inconsistencies will be dealt with over time.

## Table of Contents

The Contents are based on a comparison of several learning resources, including books, websites, and MOOCs. This learning path will develop and change over time.

Please submit ideas, suggestions, corrections, and exercises through the Issue Tracker.

**Pre-requisites** [»](./content/0.0-Prerequisites.md)
* What is Python? [»](./content/0.0-Prerequisites.md#what-is-python)
* What do people do with Python? [»](./content/0.0-Prerequisites.md#what-do-people-do-with-python)
* Suggestions [»](./content/0.0-Prerequisites.md#suggestions)
* Download and Install [»](./content/0.0-Prerequisites.md#download-and-install)
* IDEs [»](./content/0.0-Prerequisites.md#ides)
* What should I know before I start programming? [»](./content/0.0-Prerequisites.md#what-should-i-know-before-i-start-programming)

**Fundamentals** (Weeks 1--2) [»](./content/1.0-Fundamentals.md)
* First Steps [»](./content/1.0-Fundamentals.md#first-steps)
* String Manipulation [»](./content/1.0-Fundamentals.md#string-manipulation)
* Input and Print Functions [»](./content/1.0-Fundamentals.md#input-and-print-functions)
* Variable Naming Rules [»](./content/1.0-Fundamentals.md#variable-naming-rules)
* Data Types [»](./content/1.0-Fundamentals.md#data-types)
* Converting types [»](./content/1.0-Fundamentals.md#converting-types)
* Mathematical Operations in Python [»](./content/1.0-Fundamentals.md#mathematical-operations)

**Consolidation Exercises 1** [»](./content/1.2-Consolidation-Exercises.md)

**Basics** (Weeks 3--7) [»](./content/2.0-Basics.md)
* Control Flow: Conditional Operators [»](./content/2.0-Basics.md#control-flow-conditional-operators)
* Multiple if statements [»](./content/2.0-Basics.md#multiple-if-statements)
* Enter the Module [»](./content/2.0-Basics.md#enter-the-module)
* Randomness [»](./content/2.0-Basics.md#randomness)
* List Structure [»](./content/2.0-Basics.md#list-structure)

**Consolidation Exercises 2** [»](./content/2.1-Consolidation-Exercises.md)

**Basics (continued)** [»](./content/2.2-Basics2.md)
* Loops [»](./content/2.2-Basics2.md#loops)
* Functions [»](./content/2.2-Basics2.md#making-functions-do-more)
* Sets [»](./content/2.2-Basics2.md#sets)
* Dictionaries [»](./content/2.2-Basics2.md#dictionaries)
* Functions and `return` [»](./content/2.2-Basics2.md#functions-and-return)

**Consolidation Exercises 3** [»](./content/2.3-Consolidation-Exercises.md)

**Intermediate** (Weeks 8--12) [»](./content/3.0-Intermediate.md)
* Classes and Objects [»](./content/3.0-Intermediate.md#classes-and-objects)
* Class Variables [»](./content/3.0-Intermediate.md#class-variables)
* Class Methods [»](./content/3.0-Intermediate.md#class-methods)
* Special and Magic Methods [»](./content/3.0-Intermediate.md#special-and-magic-methods)
* Subclasses and Inheritance [»](./content/3.0-Intermediate.md#subclasses-and-inheritance)
* Importing Modules [»](./content/3.0-Intermediate.md#importing-modules)
* Back to Basics: Turtle [»](./content/3.0-Intermediate.md#back-to-basics-turtle)

**Advanced** (Weeks 13--?)

**Solutions** [»](./content/9.0-Solutions.md)

**Glossary**

## External Resources / Bibliography

* If you want to learn from people who know what they are doing [»](EXTERNAL.md)

## Credits

* [Tabler Icons](https://tabler-icons.io/)
* [gramma: command-line grammar checker](https://caderek.github.io/gramma/)
