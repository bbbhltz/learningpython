# External Resources for Learning Python

## The Roadmap

source: [Learn to become a modern Python developer](https://roadmap.sh/python)

![Python Roadmap](https://roadmap.sh/roadmaps/python.png)

## Official

* [BeginnersGuide - Python Wiki](https://wiki.python.org/moin/BeginnersGuide)

## Other Resources

* [Python Cheat Sheet](https://overapi.com/python) (via OverAPI: links to official documentation)
* [Awesome Python](https://awesome-python.com/)
* [Automate the Boring Stuff with Python](https://automatetheboringstuff.com/)
* [Think Python 2e](https://greenteapress.com/wp/think-python-2e/)
* [Non-Programmer's Tutorial for Python 3](https://en.wikibooks.org/wiki/Non-Programmer%27s_Tutorial_for_Python_3)
* [Learn Python Coding - The Python Coding Book](https://thepythoncodingbook.com/)
* [Python Tutorial on W3Schools](https://www.w3schools.com/python/default.asp)
* [Cheat Sheets - Python Crash Course, 2nd Edition](https://ehmatthes.github.io/pcc_2e/cheat_sheets/cheat_sheets/)
* [Tiny Python Projects](http://tinypythonprojects.com/)
* [Which "Full Python course" Youtube video is the best? on /r/learnpython](https://teddit.net/r/learnpython/comments/ru0jbu/which_full_python_course_youtube_video_is_the_best/hqxjek7/#c)
