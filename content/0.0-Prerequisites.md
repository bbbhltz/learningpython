[![](../tabler-icon-home.svg)](../README.md)

# Pre-Requisites

## What is Python?

Python is a high level, general purpose programming created by Guido Van Rossum. It was publicly released in 1991.

**High level** means that certain details are hidden from the programmer[^highlevel]:

> A high level language is a programming language which abstracts the execution semantics of a computer architecture from the specification of the program.
This abstraction make the process of developing a program a much more simple and understandable process. The amount of abstraction that is provided often defines how 'high level' a programming language is.

Python is seen as an **easy** language to start with. That is why I am starting with Python; I know nothing about programming.

Python works across different operating systems, and there are many libraries and packages available: See [PyPI: The Python Package Index](https://pypi.org/).

## What do people do with Python?

They solve problems!

Most users, like myself, are probably familiar with many Python applications without knowing it. Python can be used to create web apps, Android apps, different types of user interfaces, games, and powerful console applications.

Companies use Python too. Dropbox, Reddit, Alphabet (Google and YouTube), Yahoo!, NASA, Facebook, Instagram, Industrial Light & Magic, and Mozilla are just some examples[^examples].

## Suggestions

There are many transferable suggestions from my experience as a language teacher that can be applied to mearning a programming language.

* Whenever you are experimenting with something new, you should try to make mistakes.
* If you are learning with videos, use two screens.
* Don't worry if you don't get it. Take a break, work on something else.
* Working on something for a couple hours per week is good, but not really enough. Maintaining your level of a language is different for every person. Personally, if I don't work on something every day, I don't feel like I am making progress. I suggest working a regular amount every day, but more than two hours per week. Ideally, an hour a day would be a good starting point.
* *more coming soon*

## Download and Install

On [Python.org](https://www.python.org/) you can find sufficient instructions to install Python. I do not use Windows or Mac, but the process is fairly straightforward. Downloads > Windows > Windows Installer:

![Windows Installer](./images/download.png "Downloading Windows installer for Python")

### IDEs

There are dozens of ways to write Python scripts and programs.

![](./images/idle.png "IDLE")

A nice default is [IDLE](https://docs.python.org/3/library/idle.html). It has the basic features that a beginner needs:

* ability to write scripts
* ability to interact with a shell

For the moment, I am partial to [Thonny](https://thonny.org/) because it looks OK, and it is made for beginners. I am a beginner, so it seems natural to use it.

My second choice is [VSCodium](https://vscodium.com/). It is more than a Python IDE, but it remains simple and light.

Other very popular choices are:

* [PyCharm by JetBrains](https://www.jetbrains.com/pycharm/download/) (the Community Edition). It has beginner and expert features.
* [Browser-based IDE: Replit](https://replit.com/)
* [PyDev](https://www.pydev.org/)
* [Spyder IDE](https://www.spyder-ide.org/)

Or, you can just learn using online tools like [trinket](https://trinket.io/python3/796212e08d).

[See My Thonny Setup](0.2-ThonnySetup.md)

## What should I know before I start programming?

It might be interesting for you to be familiar with *Computer Science Fundamentals*.

As of writing, I have not had any training in this.

These fundamentals would include, for example:

* Decision trees,
* Parallelism,
* Order and Search,
* Naming,
* Abstraction,
* and Algorithmic Thinking.

Those are some of the tools we use, without thinking about it, to solve problems.

[Click here for more information on Computer Science Fundamentals](0.1-CSFundamentals.md)

[![](../tabler-icon-home.svg)](../README.md)

[^highlevel]: from [HThreads - RD Glossary](https://web.archive.org/web/20070826224349/http://www.ittc.ku.edu/hybridthreads/glossary/index.php)
[^examples]: from [Python (programming language) on Wikipedia](https://en.wikipedia.org/wiki/Python_(programming_language)#Popularity)
