[![](../tabler-icon-home.svg)](../README.md)

# Consolidation Exercises for Python Fundamentals

## Party Calculations

You are planning for a party.

All invitees agree to split the bill equally.

**BUT**

There are a few people who don't drink alcohol.

**Create a calculator to "split the bill".**

* It needs to take into account food costs and alcohol costs.
* Drinkers pay their share of everything.
* Non-drinkers only pay their share of the food.

## Character Counting

**Create a script that counts the number of characters entered and produces an output similar to this:**

```
Enter text below:
Python is an interpreted high-level general-purpose programming language. Its design philosophy emphasizes code readability with its use of significant indentation. Its language constructs as well as its object-oriented approach aim to help programmers write clear, logical code for small and large-scale projects.
Your text, "Python is an interpreted high-l...small and large-scale projects." is 314 characters long.
```

* The starting and ending of the entered text must represent 10% of the total characters.

## Pointless Mathematics

You have been asked to help someone with a math problem.

Their teacher has asked them the following question:

**Part 1: If you draw a perfect circle on a piece of A4 paper, where the circle's diameter is the same as the paper's width, and then cut away the paper that is not in the circle, how much paper do you waste?**

Notes:

* A piece of A4 paper has a width of 21cm and a height of 29.7cm
* The area of a circle can be calculated using the formula below:

```math
\pi r^2
```

* We can create an approximate for pi by creating a variable with a value of 3.14159

**Part 2: If you inscribe an equilateral triangle in that circle, what will it's area be?**

![Triangle in a circle](https://etc.usf.edu/clipart/43400/43436/3c_43436_md.gif "Equilaterl Triangle Inscribed in Circle")

Notes:

* The length of a side in this case can be calculated using the formula below:

```math
\text{Length of side} = \frac{\sqrt{3}}{2} \times \text{radius} \times 2
```

* **Remember!**

```math
\sqrt{n} = n^\frac{1}{2}
```

* The area of an equilateral triangle can be calculated using the formula below:

```math
\frac{\text{Triangle Side}^2 \cdot \sqrt{3}}{4}
```

You can watch this video to understand the math: [Link](https://youtu.be/UmiZK6Hgm6c).

## Solutions

[![](./images/solutionbutton.svg)](9.0-Solutions.md#consolidation-solutions)

[![](../tabler-icon-home.svg)](../README.md)
