# Turtle Racer!

import turtle
import random

turtle.bgcolor("black")
sewer = turtle.Turtle()
sewer.pen(pencolor="gray", pensize=5, speed=10)
lspeed = random.randint(1, 10)
leo = turtle.Turtle()
leo.shape("turtle")
leo.pen(pencolor="blue", pensize=2, speed=lspeed, fillcolor="blue")
rspeed = random.randint(1, 10)
raph = turtle.Turtle()
raph.shape("turtle")
raph.pen(pencolor="red", pensize=2, speed=rspeed, fillcolor="red")
dspeed = random.randint(1, 10)
donny = turtle.Turtle()
donny.shape("turtle")
donny.pen(pencolor="purple", pensize=2, speed=dspeed, fillcolor="purple")
mspeed = random.randint(1, 10)
mikey = turtle.Turtle()
mikey.shape("turtle")
mikey.pen(pencolor="orange", pensize=2, speed=mspeed, fillcolor="orange")

sewer.hideturtle()


def speedreset():
    lspeed = random.randint(0, 11)
    rspeed = random.randint(0, 11)
    speed = random.randint(0, 11)
    mspeed = random.randint(0, 11)


def large_rectangle():
    sewer.forward(300)
    sewer.left(90)
    sewer.forward(400)
    sewer.left(90)
    sewer.forward(600)
    sewer.left(90)
    sewer.forward(400)
    sewer.left(90)
    sewer.forward(300)


def small_rectangle():
    sewer.forward(200)
    sewer.left(90)
    sewer.forward(200)
    sewer.left(90)
    sewer.forward(400)
    sewer.left(90)
    sewer.forward(200)
    sewer.left(90)
    sewer.forward(200)


sewer.fillcolor("grey")
sewer.begin_fill()
large_rectangle()
sewer.end_fill()

sewer.up()
sewer.left(90)
sewer.forward(100)
sewer.right(90)
sewer.down()

sewer.fillcolor("black")
sewer.begin_fill()
small_rectangle()
sewer.end_fill()

leo.up()
leo.goto(0, 80)
leo.down()

raph.up()
raph.goto(0, 60)
raph.down()

donny.up()
donny.goto(0, 40)
donny.down()

mikey.up()
mikey.goto(0, 20)
mikey.down()

# 1st leg

turtles = [1, 2, 3, 4]

for i in range(4):
    randomturtle = random.choice(turtles)
    if randomturtle == 1:
        leo.forward(220)
    if randomturtle == 2:
        raph.forward(240)
    if randomturtle == 3:
        donny.forward(260)
    if randomturtle == 4:
        mikey.forward(280)
    turtles.remove(randomturtle)

leo.left(90)
raph.left(90)
donny.left(90)
mikey.left(90)

# 2nd leg

speedreset()

turtles = [1, 2, 3, 4]

for i in range(4):
    randomturtle = random.choice(turtles)
    if randomturtle == 1:
        leo.forward(240)
    if randomturtle == 2:
        raph.forward(280)
    if randomturtle == 3:
        donny.forward(320)
    if randomturtle == 4:
        mikey.forward(360)
    turtles.remove(randomturtle)

leo.left(90)
raph.left(90)
donny.left(90)
mikey.left(90)

# 3rd leg

speedreset()

turtles = [1, 2, 3, 4]

for i in range(4):
    randomturtle = random.choice(turtles)
    if randomturtle == 1:
        leo.forward(440)
    if randomturtle == 2:
        raph.forward(480)
    if randomturtle == 3:
        donny.forward(520)
    if randomturtle == 4:
        mikey.forward(560)
    turtles.remove(randomturtle)

leo.left(90)
raph.left(90)
donny.left(90)
mikey.left(90)

# 4th leg

speedreset()

turtles = [1, 2, 3, 4]

for i in range(4):
    randomturtle = random.choice(turtles)
    if randomturtle == 1:
        leo.forward(240)
    if randomturtle == 2:
        raph.forward(280)
    if randomturtle == 3:
        donny.forward(320)
    if randomturtle == 4:
        mikey.forward(360)
    turtles.remove(randomturtle)

leo.left(90)
raph.left(90)
donny.left(90)
mikey.left(90)

# 5th leg

speedreset()

turtles = [1, 2, 3, 4]

for i in range(4):
    randomturtle = random.choice(turtles)
    if randomturtle == 1:
        leo.forward(220)
    if randomturtle == 2:
        raph.forward(240)
    if randomturtle == 3:
        donny.forward(260)
    if randomturtle == 4:
        mikey.forward(280)
    turtles.remove(randomturtle)

turtle.exitonclick()
