[![](../tabler-icon-home.svg)](../README.md)

# Thonny

Official Website: [Thonny, Python IDE for beginners](https://thonny.org/)
Github: [GitHub - thonny/thonny: Python IDE for beginners](https://github.com/thonny/thonny/)

There are plenty of great Python IDEs available. Choose the one that works for you. Honestly, many notepad-type apps can do the trick. I chose Thonny.

Thonny is aimed at beginners, but it is quite small and launches quickly. From the moment it opens, it is fairly clear how things work. Write something; press button; see output.

That is not to say that it is complete, however. There is a very (**very**) key function missing from Thonny for the moment: **Auto-bracketing**. Many apps that can be used for editing code have an option to automatically type the closing bracket, parenthesis or quotation as soon as you type the opening.

Here are the options in Geany (a default IDE that comes standard with many Linux distributions):

![Geany Auto-Bracket Options](./images/geany.png "Geany IDE Auto-Bracket Options")

and, here they are in Featherpad (the default text editor on my laptop)

![Featherpad Auto-Bracket Options](./images/featherpad.png "Featherpad Auto-Bracket Options")

Thonny does not have this feature. If you are familiar with coding, then you know that auto-closing brackets and quotations is considered a basic hallmark and standard feature.

The developer of Thonny, Aivar Annamaa, has listed this function in his [todo list](https://github.com/thonny/thonny/issues/1942#issuecomment-903065281) for [version 4.0](https://github.com/thonny/thonny/milestone/20).

Once you get over the lack of auto-bracketing, you will quickly notice that Thonny does not have any kind of hinting that other IDEs do ([docstrings](https://www.geeksforgeeks.org/python-docstrings/)). That is unfortunate, because it is aimed at beginners and should offer such an option. In order to access documentation strings, you can use the shell and ask for help using `help()`. For example, `help(print)` outputs

```
>>> help(print)
Help on built-in function print in module builtins:

print(...)
    print(value, ..., sep=' ', end='\n', file=sys.stdout, flush=False)
    
    Prints the values to a stream, or to sys.stdout by default.
    Optional keyword arguments:
    file:  a file-like object (stream); defaults to the current sys.stdout.
    sep:   string inserted between values, default a space.
    end:   string appended after the last value, default a newline.
    flush: whether to forcibly flush the stream.
```

## Setup

I have made no specific modifications to the setup. When I launch Thonny, it looks like this:

![Thonny Main Window](./images/thonnymain.png "Thonny Main Window")

1. The editor
2. The shell / output
3. Variables

I have set the theme and font as below (that font because I already use it in my terminal and text editor):

![Thonny Theme Options](./images/thonnytheme.png "Thonny Theme Options")

And, I have changed the assistant behaviour. Not because it isn't useful, but because it takes up screen real estate. I do use it when I need it.

![Thonny Assistant Options](./images/thonnyassopts.png "Thonny Assistant Options")

Finally, I have also installed two packages through the `Tools > Manage Packages` options:

**Black Code Formatter** ([info](https://github.com/psf/black))

![Black Install](./images/thonnyblack.png "Thonny Black Installation")

![Black Menu](./images/thonnyblackmenu.png "Thonny Black Menu")

**Block Highlighting**

![Block Highlighting](./images/thonnyblock.png "Thonny Block Highlighting")

[![](../tabler-icon-home.svg)](../README.md)
